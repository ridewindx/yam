YAM - YAML 1.2 parser / dumper for C++
=================================================

YAM is an C++ implementation of [YAML](http://yaml.org/).
It is a port of [JS-YAML](https://github.com/nodeca/js-yaml),
and presents the same intuitive APIs as [nlohmann/json](https://github.com/nlohmann/json).
