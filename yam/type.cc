#include <yam/type.h>

#include <yam/type/str.h>
#include <yam/type/seq.h>
#include <yam/type/map.h>

#include <yam/type/null.h>
#include <yam/type/bool.h>
#include <yam/type/int.h>
#include <yam/type/float.h>

#include <yam/type/merge.h>

#include <yam/type/omap.h>
#include <yam/type/pairs.h>
#include <yam/type/set.h>

namespace yam {

vector<TypePtr> Type::implicitTypes = {
    make_shared<NullType>(),
    make_shared<BoolType>(),
    make_shared<IntType>(),
    make_shared<FloatType>(),
    make_shared<MergeType>(),
};

unordered_map<string, TypePtr> Type::typeMap = {
    {StrType().tag, make_shared<StrType>()},
    {SeqType().tag, make_shared<SeqType>()},
    {MapType().tag, make_shared<MapType>()},
    {NullType().tag, make_shared<NullType>()},
    {BoolType().tag, make_shared<BoolType>()},
    {IntType().tag, make_shared<IntType>()},
    {FloatType().tag, make_shared<FloatType>()},
    {MergeType().tag, make_shared<MergeType>()},
    {OmapType().tag, make_shared<OmapType>()},
    {PairsType().tag, make_shared<PairsType>()},
    {SetType().tag, make_shared<SetType>()}
};

}
