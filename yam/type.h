#pragma once

#include <string>
#include <functional>
#include <memory>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include <yam/yaml.h>

namespace yam {

using namespace std;

struct Type;
using TypePtr = shared_ptr<Type>;

struct Type {
    string tag;
    string kind;
    function<bool (const yaml&)> resolve = [](const yaml&) { return true; };
    function<yaml (const yaml&)> construct = [](const yaml& data) { return data; };

    Type(string &&tag, string &&kind) : tag(std::move(tag)), kind(std::move(kind)) {}

    static vector<TypePtr> implicitTypes;
    static unordered_map<string, TypePtr> typeMap;
};

extern vector<TypePtr> types;

}
