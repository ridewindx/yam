#pragma once

#include <json.hpp>

namespace yam {

using yaml = nlohmann::json;

std::vector<yaml> loadAll(std::string&& input);

yaml load(std::string&& input);

}
