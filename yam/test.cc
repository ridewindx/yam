#include <yam/yaml.h>
#include <yam/loader.h>
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char** argv) {
    std::ifstream ifs(argv[1]);
    std::string content((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
    auto y = yam::load(std::move(content));

    cout << y.dump(2) << endl;

    return 0;
}
