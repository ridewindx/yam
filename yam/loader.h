#pragma once

#include <string>
#include <vector>

#include <yam/yaml.h>

namespace yam {

std::vector<yaml> loadAll(std::string&& input);

yaml load(std::string&& input);

}
