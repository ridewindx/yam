#include <yam/type.h>

namespace yam {

struct PairsType : Type {
    PairsType() : Type("tag:yaml.org,2002:pairs", "sequence") {
        resolve = [](const yaml& data) {
            if (data.is_null())
                return true;

            for (auto& pair: data) {
                if (not pair.is_object())
                    return false;

                if (pair.size() != 1)
                    return false;
            }

            return true;
        };

        construct = [](const yaml& data) {
            yaml result;
            for (auto& pair: data)
                result.push_back({pair.cbegin().key(), pair.cbegin().value()});

            return result;
        };
    }
};

}
