#include <yam/type.h>

namespace yam {

struct BoolType : Type {
    BoolType() : Type("tag:yaml.org,2002:bool", "scalar") {
        resolve = [](const yaml& data) {
            if (data.is_null())
                return false;

            return data == "true" or data == "True" or data == "TRUE" or
                   data == "false" or data == "False" or data == "FALSE";
        };

        construct = [](const yaml& data) {
            return data == "true" or data == "True" or data == "TRUE";
        };
    }
};

}
