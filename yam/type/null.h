#include <yam/type.h>

namespace yam {

struct NullType : Type {
    NullType() : Type("tag:yaml.org,2002:null", "scalar") {
        resolve = [](const yaml& data) {
            if (data.is_null())
                return true;

            return data == "~" or data == "null" or data == "Null" or data == "NULL";
        };

        construct = [](const yaml&) {
            return yaml();
        };
    }
};

}
