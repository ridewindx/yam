#include <yam/type.h>

namespace yam {

struct SeqType : Type {
    SeqType() : Type("tag:yaml.org,2002:seq", "sequence") {
        construct = [](const yaml& data) {
            return not data.is_null() ? data : yaml::array();
        };
    }
};


}
