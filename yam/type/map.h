#include <yam/type.h>

namespace yam {

struct MapType: Type {
    MapType() : Type("tag:yaml.org,2002:map", "mapping") {
        construct = [](const yaml& data) {
            return not data.is_null() ? data : yaml::object();
        };
    }
};

}
