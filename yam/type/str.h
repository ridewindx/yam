#include <yam/type.h>

namespace yam {

struct StrType: Type {
    StrType() : Type("tag:yaml.org,2002:str", "scalar") {
        construct = [](const yaml& data) {
            return not data.is_null() ? data : "";
        };
    }
};

}
