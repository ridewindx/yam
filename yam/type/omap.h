#include <yam/type.h>

#include <unordered_set>

namespace yam {

struct OmapType: Type {
    OmapType() : Type("tag:yaml.org,2002:omap", "sequence") {
        resolve = [](const yaml& data) {
            if (data.is_null())
                return true;

            unordered_set<string> keys;
            for (auto& pair: data) {
                if (not pair.is_object())
                    return false;

                if (pair.size() != 1)
                    return false;

                auto key = pair.cbegin().key();

                if (keys.count(key))
                    return false;

                keys.insert(key);
            }

            return true;
        };

        construct = [](const yaml& data) {
            return not data.is_null() ? data : yaml::array();
        };
    }
};

}
