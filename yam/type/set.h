#include <yam/type.h>

namespace yam {

struct SetType : Type {
    SetType() : Type("tag:yaml.org,2002:set", "mapping") {
        resolve = [](const yaml& data) {
            if (data.is_null())
                return true;

            for (auto it = data.cbegin(); it != data.cend(); ++it) {
                if (not it.value().is_null())
                    return false;
            }

            return true;
        };

        construct = [](const yaml& data) {
            return not data.is_null() ? data : yaml::object();
        };
    }
};

}
