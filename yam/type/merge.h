#include <yam/type.h>

namespace yam {

struct MergeType : Type {
    MergeType() : Type("tag:yaml.org,2002:merge", "scalar") {
        resolve = [](const yaml& data) {
            return data == "<<" or data.is_null();
        };
    }
};

}
